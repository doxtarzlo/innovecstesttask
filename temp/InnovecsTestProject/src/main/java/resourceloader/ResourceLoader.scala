package resourceloader

import java.io.{BufferedReader, InputStream, InputStreamReader}

/**
  * Created by anton on 08.03.2017.
  */
trait ResourceLoader {

  def load(resourceName: String): BufferedReaderIterator

  def getBufferedReader(inputStream: InputStream): BufferedReader = {
    new BufferedReader(new InputStreamReader(inputStream))
  }

  def getInputStream(resourceName: String): InputStream = {
    getClass.getClassLoader.getResourceAsStream(resourceName)
  }

}
