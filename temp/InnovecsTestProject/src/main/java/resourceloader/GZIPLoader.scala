package resourceloader

import java.io.{BufferedReader, IOException}
import java.util.zip.GZIPInputStream

/**
  * Created by anton on 08.03.2017.
  */
class GZIPLoader extends ResourceLoader {

  @throws(classOf[IOException])
  override def load(resourceName: String): BufferedReaderIterator = {
    try {
      new BufferedReaderIterator(getBufferedReader(new GZIPInputStream(getInputStream(resourceName))))
    } catch {
      case ioe: IOException =>
        println("Didn't manage to open .gz file")
        ioe.printStackTrace()
        throw ioe
    }
  }

}
