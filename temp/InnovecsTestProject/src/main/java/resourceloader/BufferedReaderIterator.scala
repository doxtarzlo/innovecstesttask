package resourceloader

import java.io.BufferedReader

/**
  * Created by anton on 08.03.2017.
  */
class BufferedReaderIterator(reader: BufferedReader) extends Iterator[String] {
  override def hasNext: Boolean = reader.ready()
  override def next(): String = reader.readLine()
}