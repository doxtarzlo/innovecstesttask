package resourcewriter

import java.io.IOException
import java.nio.charset.{Charset, StandardCharsets}
import java.nio.file.{Files, Paths}

/**
  * Created by anton on 08.03.2017.
  */
class ResourceWriter {

  private var targePath = "src/main/resources/"

  private var charset = StandardCharsets.UTF_8

  def targetPath(targetPath: String): ResourceWriter = {
    this.targePath = targetPath
    this
  }

  def withCharset(charset: Charset): ResourceWriter = {
    this.charset = charset
    this
  }

  @throws(classOf[IOException])
  def write(content: String, name: String): Boolean = {
    try {
      Files.write(Paths.get(targePath + name), content.getBytes(charset))
      true
    } catch {
      case ioe: IOException =>
        println("Didn't manage to write file")
        throw ioe
    }
  }

}
