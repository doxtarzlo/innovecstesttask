package resourceresolver

import scala.collection.mutable.ListBuffer

/**
  * Created by anton on 08.03.2017.
  */
class CsvResolver {

  private var hasHeader: Boolean = false
  private var iterator: Iterator[String] = _

  def withHeader(hasHeader: Boolean): CsvResolver = {
    this.hasHeader = hasHeader
    this
  }

  def resolve(iterator: Iterator[String]): CsvResolver = {
    this.iterator = iterator
    this
  }

  def asMap(columnName: String*): ListBuffer[Map[String, String]] = {
    var list = ListBuffer[Map[String, String]]()

    iterator.drop(if (hasHeader) 0 else 1).foreach(entry => {
      val columns =
        entry
          .split(",")
          .map(_.trim.stripPrefix("\"").stripSuffix("\""))

      var map = Map[String, String]()

      columnName.zipWithIndex.foreach(entry => map += entry._1 -> s"${columns(entry._2)}")

      list += map

    })

    list
  }

}
