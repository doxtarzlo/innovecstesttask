import resourceloader.GZIPLoader
import resourceresolver.CsvResolver
import resourcewriter.ResourceWriter

import scala.collection.mutable.ListBuffer

/**
  * Created by anton on 04.03.2017.
  */
object Main {

  // Please, let me know if some comments are required

  def main(args: Array[String]): Unit = {

    /** It would be great to get csv content as map of deserialized data
      * That's why I decided to use "product-collections" library: https://github.com/marklister/product-collections
      * It didn't work as data is saved as string in .gz file

      * As mentioned library is essentially useless, I decided to write own implementation of .csv parser
      * I'm aware of several methods how we can get Iterator[String] of file from resources:
      * - Source.fromResources("").getLines
      * - Source.fromInputStream(getClass.getResourceAsStream(""), "").getLines
      * It did not work for some reason. To avoid wasting time in deep research, I implemented own loader
      * */
    val iterator = new GZIPLoader().load("planes_log.csv.gz")
    val list = new CsvResolver()
      .withHeader(false)
      .resolve(iterator)
      .asMap("YEAR", "QUARTER", "MONTH", "DAY_OF_MONTH", "DAY_OF_WEEK", "FLIGHT_DATE", "ORIGIN", "DESTINATION")

    val writer = new ResourceWriter()

    writer.write(task1(list), "task1.txt")
    writer.write(task2(list), "task2.txt")
    writer.write(task3(list), "task3.txt")

  }

  def task1(list: ListBuffer[Map[String, String]]): String = {
    task1(list, null)
  }

  def task1(list: ListBuffer[Map[String, String]], prefix: String): String = {
    intoString((builder) => {
      list.groupBy(_.get("DESTINATION")).toSeq.sortWith(_._2.size > _._2.size).zipWithIndex.foreach(entry => {
        val result = entry._2 + 1 + ". " + entry._1._1.get + ": " + entry._1._2.size
        builder.append(if (prefix != null) prefix + result else result).append("\r\n")
      })
    })
  }

  def task2(list: ListBuffer[Map[String, String]]): String = {
    val origin = list.groupBy(_.get("ORIGIN"))
    val destination = list.groupBy(_.get("DESTINATION"))

    var map = Map[String, Int]()

    destination.foreach(entry => {
      val diff = entry._2.size - origin(entry._1).size
      if (diff != 0) map += entry._1.get -> diff
    })

    intoString((builder) =>
      map.toSeq.sortWith(_._2 < _._2).zipWithIndex.foreach(entry => {
        builder.append(entry._2 + 1 + ". " + entry._1._1 + ": " + entry._1._2).append("\r\n")
      })
    )

  }

  def task3(list: ListBuffer[Map[String, String]]): String = {
    val (firstWeek, nextWeeks) = list.splitAt(list.indexWhere(entry => entry("DAY_OF_WEEK").toInt == 7))

    intoString((builder) => {
      (firstWeek :: nextWeeks.grouped(7).toList).zipWithIndex.foreach(entry => {
        builder.append("W" + (entry._2 + 1)).append("\r\n").append(task1(entry._1, "\t"))
      })
    })

  }

  def intoString(task: (StringBuilder) => Unit): String = {
    val builder = new StringBuilder
    task.apply(builder)
    builder.toString()
  }

}
