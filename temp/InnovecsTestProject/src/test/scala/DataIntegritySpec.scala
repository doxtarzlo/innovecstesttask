import org.specs2.mutable.Specification
import resourceloader.GZIPLoader
import resourceresolver.CsvResolver

import scala.collection.mutable.ListBuffer

/**
  * Created by anton on 09.03.2017.
  */
class DataIntegritySpec extends Specification {

  override def is =
    s2"""
        This is Specification to check data integrity which comes from planes_log.csv.gz file

        Provided .csv file doesn't not contain empty cells $hasEmptyCells

        'YEAR' column contains 4 digit numbers without decimal places $yearData

        'QUARTER' column contains numbers from 1 to 4 $quarterData

        'MONTH' column contains numbers from 1 to 12 $monthData

        'DAY_OF_MONTH' column contains numbers from 1 to 31 $dayOfMonthData

        'DAY_OF_WEEK' column contains numbers from 1 to 7 $dayOfWeekData

        'FLIGHT_DATE' column contain strings in format dd.MM.yyyy $flightDateData

        'ORIGIN' column contains 3 char strings $originData

        'DESTINATION' contains 3 char strings $destinationData
      """

  var list: ListBuffer[Map[String, String]] = _

  {
    list = new CsvResolver()
      .withHeader(true)
      .resolve(new GZIPLoader().load("planes_log.csv.gz"))
      .asMap("YEAR", "QUARTER", "MONTH", "DAY_OF_MONTH", "DAY_OF_WEEK", "FLIGHT_DATE", "ORIGIN", "DESTINATION")
  }

  def hasEmptyCells: Boolean = {
    val dataList = list.flatten.map(_._2)
    !dataList.contains("") | !dataList.contains(null)
  }

  def yearData: Boolean = {
    verify("YEAR", "[0-9]{4}")
  }

  def quarterData: Boolean = {
    verify("QUARTER", "[1-4]{1}")
  }

  def monthData: Boolean = {
    verify("MONTH", "1[0-2]|[1-9]")
  }

  def dayOfMonthData: Boolean = {
   verify("DAY_OF_MONTH", "[1-9]|[12]\\d|3[01]")
  }

  def dayOfWeekData: Boolean = {
    verify("DAY_OF_WEEK", "[1-7]{1}")
  }

  def flightDateData: Boolean = {
    verify("FLIGHT_DATE", "\\d{4}\\-\\d{2}\\-\\d{2}") // I'll keep it simple, there are a lots of expressions for date validation in internet
  }

  def originData: Boolean = {
    verify("ORIGIN", "[A-Z]{3}")
  }

  def destinationData: Boolean = {
    verify("DESTINATION", "[A-Z]{3}")
  }

  def verify(columnName: String, regex: String): Boolean = {
    list.drop(1).foreach(entry => {
      if (!entry(columnName).matches(regex))
        return false
    })
    true
  }

}
