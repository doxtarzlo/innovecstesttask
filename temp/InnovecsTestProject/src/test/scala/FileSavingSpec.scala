import java.io.IOException
import java.nio.file.{AccessDeniedException, NoSuchFileException}

import org.specs2.mutable.Specification
import resourcewriter.ResourceWriter

import scala.io.Source

/**
  * Created by anton on 10.03.2017.
  */
class FileSavingSpec extends Specification {

  override def is =
    s2"""
        This is Specification to check writing capabilities of ResourceWriter.class

        ResourceWriter is able to save files with name and specified extension into existing directory $positiveSaving

        ResourceWriter throws message if there is attempt to save file into non-existing directory $nonExistingDir

        ResourceWriter throws message if there is attempt to save file with empty name $emptyName

        ResourceWriter throws message if there is attempt to save file without extension

         """"

  val writer: ResourceWriter = new ResourceWriter().targetPath("src/test/resources/")

  def positiveSaving: Boolean = {
    val content = "test"
    val name = "positiveSaving.txt"
    writer.write(content, name)
    Thread.sleep(10000)
    Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(name)).getLines.mkString.equals(content)
  }

  def nonExistingDir: Boolean = {
    writer.targetPath("fake_path/").write("test", "nonExistingDir.txt") must throwA(new NoSuchFileException("fake_path\\nonExistingDir.txt"))
  }

  def emptyName: Boolean = {
    writer.targetPath("src/test/resources/").write("test", "") must throwA(new AccessDeniedException("src\\test\\resources"))
  }

  def emptyExtension: Boolean = {
    writer.write("test", "emptyName") must throwA(new AccessDeniedException("src\\test\\resources"))
  }

}
